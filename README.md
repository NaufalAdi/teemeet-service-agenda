# teemeet-service-agenda

master<br>
[![pipeline status](https://gitlab.com/NaufalAdi/teemeet-service-agenda/badges/master/pipeline.svg)](https://gitlab.com/NaufalAdi/teemeet-service-agenda/-/commits/master)
[![coverage report](https://gitlab.com/NaufalAdi/teemeet-service-agenda/badges/master/coverage.svg)](https://gitlab.com/NaufalAdi/teemeet-service-agenda/-/commits/master)
<br>dev<br>
[![pipeline status](https://gitlab.com/NaufalAdi/teemeet-service-agenda/badges/dev/pipeline.svg)](https://gitlab.com/NaufalAdi/teemeet-service-agenda/-/commits/dev)
[![coverage report](https://gitlab.com/NaufalAdi/teemeet-service-agenda/badges/dev/coverage.svg)](https://gitlab.com/NaufalAdi/teemeet-service-agenda/-/commits/dev)
<br>
### Naufal Adi Wijanarko - 1906305871

Sepertinya ada kesalahan dalam tampilan coverage karena jika dijalankan test di local sudah 100% dan semua line sudah tercover
![](images/coverage.JPG)
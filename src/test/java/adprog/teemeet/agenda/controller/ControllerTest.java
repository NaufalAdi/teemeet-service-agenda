package adprog.teemeet.agenda.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import adprog.teemeet.agenda.core.DayDTO;
import adprog.teemeet.agenda.core.MonthDTO;
import adprog.teemeet.agenda.model.Event;
import adprog.teemeet.agenda.service.EventServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = AgendaController.class)
class ControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventServiceImpl eventService;

    private DayDTO dayDTO;

    private MonthDTO monthDTO;

    private Event event;

    /**
     * This method will run before each tests.
     */
    @BeforeEach
    public void setUp() {
        event = new Event();
        event.setEventId(0);
        event.setName("Dummy");
        event.setDescription("Dummy");
        event.setDate(LocalDate.parse("2017-01-13"));
        event.setTimeStart(LocalTime.parse("17:09:42.411"));
        event.setTimeEnd(LocalTime.parse("17:09:50.411"));
        event.setGroupId(0);

        dayDTO = new DayDTO();
        dayDTO.setYear(2017);
        dayDTO.setMonth(1);
        dayDTO.setDay(13);
        dayDTO.setEvents(Arrays.asList(event));

        monthDTO = new MonthDTO();
        monthDTO.setYear(2017);
        monthDTO.setMonth(1);
        monthDTO.setDayList(Arrays.asList(dayDTO));
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetAllEvents() throws Exception {
        List<Event> eventList = Arrays.asList(event);
        when(eventService.getAllEvents()).thenReturn(eventList);
        mockMvc.perform(get("/events")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].eventId").value("0"));
        verify(eventService, times(1)).getAllEvents();
    }

    @Test
    void testControllerPostEvent() throws Exception {
        when(eventService.createEvent(event)).thenReturn(event);
        mockMvc.perform(post("/events")
                .contentType(MediaType.APPLICATION_JSON).content(mapToJson(event)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Dummy"));
        verify(eventService, times(1)).createEvent(event);
    }

    @Test
    void testControllerGetEventByEventId() throws Exception {
        when(eventService.getEventByEventId(event.getEventId())).thenReturn(event);
        mockMvc.perform(get("/events/0")
            .contentType(MediaType.APPLICATION_JSON).content(mapToJson(event)))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("Dummy"));
        verify(eventService, times(1)).getEventByEventId(event.getEventId());
    }

    @Test
    void testControllerUpdateEvent() throws Exception {
        when(eventService.updateEvent(event.getEventId(), event)).thenReturn(event);
        mockMvc.perform(put("/events/0")
            .contentType(MediaType.APPLICATION_JSON).content(mapToJson(event)))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.name").value("Dummy"));
        verify(eventService, times(1)).updateEvent(0L, event);

    }

    @Test
    void testControllerDeleteEvent() throws Exception {
        mockMvc.perform(delete("/events/0").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
        verify(eventService, times(1)).deleteEventByID(0L);
    }

    @Test
    void testControllerSearchByYearMonth() throws Exception {
        when(eventService.getMonth(2017,1)).thenReturn(monthDTO);
        mockMvc.perform(get("/events/search?year=2017&month=1")
            .contentType(MediaType.APPLICATION_JSON).content(mapToJson(event)))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.month").value(1));
        verify(eventService, times(1)).getMonth(2017,1);
    }

    @Test
    void testControllerSearchByYearMonthDay() throws Exception {
        when(eventService.getEventByDate(2017,1,13)).thenReturn(dayDTO);
        mockMvc.perform(get("/events/search?year=2017&month=1&day=13")
            .contentType(MediaType.APPLICATION_JSON).content(mapToJson(event)))
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.day").value(13));
        verify(eventService, times(1))
            .getEventByDate(2017,1,13);
    }

    @Test
    void testControllerSearchByYearMonthGroupId() throws Exception {
        when(eventService.getMonthByGroupId(2017,1,0)).thenReturn(monthDTO);
        mockMvc.perform(get("/events/search?year=2017&month=1&groupId=0")
            .contentType(MediaType.APPLICATION_JSON).content(mapToJson(monthDTO)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.month").value(1));
        verify(eventService, times(1))
            .getMonthByGroupId(2017,1,0);
    }

    @Test
    void testControllerSearchByYearMonthDayGroupId() throws Exception {
        when(eventService.getEventByDateAndGroupId(2017,1,13,0))
            .thenReturn(dayDTO);
        mockMvc.perform(get("/events/search?year=2017&month=1&day=13&groupId=0")
            .contentType(MediaType.APPLICATION_JSON).content(mapToJson(dayDTO)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.day").value(13));
        verify(eventService, times(1))
            .getEventByDateAndGroupId(2017,1,13,0);
    }

    @Test
    void testControllerSearchByGroupId() throws Exception {
        ArrayList<Event> events = new ArrayList<>();
        events.add(event);
        when(eventService.getEventByGroupId(1)).thenReturn(events);
        mockMvc.perform(get("/events/search?groupId=1")
            .contentType(MediaType.APPLICATION_JSON).content(mapToJson(dayDTO)))
            .andExpect(status().isOk());
        verify(eventService, times(1))
            .getEventByGroupId(1);
    }

    @Test
    void testControllerDeleteEventByGroupId() throws Exception {
        doNothing().when(eventService).deleteEventByGroupId(1);
        mockMvc.perform(delete("/events?groupId=1"))
            .andExpect(status().isOk());
        verify(eventService, times(1)).deleteEventByGroupId(1);
    }
}

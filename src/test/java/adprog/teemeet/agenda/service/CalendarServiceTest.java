package adprog.teemeet.agenda.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalendarServiceTest {

    private CalendarServiceImpl agendaService;

    @BeforeEach
    public void setUp() {
        agendaService = new CalendarServiceImpl();
    }

    @Test
    void testAgendaServiceGetDatesinAMonthReturnValue() throws Exception {
        List res = agendaService.getDaysinAMonth(2021, 4);
        assertEquals(1,res.get(4));
        assertFalse(res.contains(31));
    }
}



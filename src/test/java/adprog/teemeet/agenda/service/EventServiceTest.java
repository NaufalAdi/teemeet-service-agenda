package adprog.teemeet.agenda.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import adprog.teemeet.agenda.core.DayDTO;
import adprog.teemeet.agenda.core.MonthDTO;
import adprog.teemeet.agenda.model.Event;
import adprog.teemeet.agenda.repository.AgendaEventRepository;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {
    @Mock
    private CalendarServiceImpl calendarService;

    @Mock
    private AgendaEventRepository agendaEventRepository;

    @InjectMocks
    private EventServiceImpl eventService;

    private Event event;

    private Event event1;

    private Event event2;

    private DayDTO dayDTO;

    private MonthDTO monthDTO;

    private ArrayList<Event> allEvents;

    private ArrayList<Event> group1Events;

    @BeforeEach
    public void setUp() throws Exception {
        allEvents = new ArrayList<>();
        group1Events = new ArrayList<>();

        event = new Event();
        event.setEventId((long)0);
        event.setName("Dummy");
        event.setDescription("Dummy");
        event.setDate(LocalDate.parse("2017-01-13"));
        event.setTimeStart(LocalTime.parse("17:09:42.411"));
        event.setTimeEnd(LocalTime.parse("17:09:50.411"));
        event.setGroupId(0);
        allEvents.add(event);

        event1 = new Event();
        event1.setEventId((long)1);
        event1.setName("Dummy1");
        event1.setDescription("Dummy2desc");
        event1.setDate(LocalDate.parse("2017-01-13"));
        event1.setTimeStart(LocalTime.parse("17:09:42.411"));
        event1.setTimeEnd(LocalTime.parse("17:09:50.411"));
        event1.setGroupId(1);
        allEvents.add(event1);
        group1Events.add(event1);

        event2 = new Event();
        event2.setEventId((long)2);
        event2.setName("Dummy2");
        event2.setDescription("Dummy2desc");
        event2.setDate(LocalDate.parse("2017-01-13"));
        event2.setTimeStart(LocalTime.parse("17:09:42.411"));
        event2.setTimeEnd(LocalTime.parse("17:09:50.411"));
        event2.setGroupId(1);
        allEvents.add(event2);
        group1Events.add(event2);

        dayDTO = new DayDTO();
        dayDTO.setYear(2017);
        dayDTO.setMonth(1);
        dayDTO.setDay(13);

        monthDTO = new MonthDTO();
        monthDTO.setYear(2021);
        monthDTO.setMonth(6);
        monthDTO.setMonthName("JUNE");
    }

    @Test
     void testCreateEvent() throws Exception {
        lenient().when(eventService.createEvent(event)).thenReturn(event);
        assertEquals(event.getName(), eventService.createEvent(event).getName());
    }

    @Test
    void testGetEventByEventId() throws Exception {
        lenient().when(agendaEventRepository.getEventByEventId((long)1)).thenReturn(event);
        assertEquals(event, eventService.getEventByEventId((long)1));
    }

    @Test
    void testGetAllEvents() throws Exception {
        when(agendaEventRepository.findAll()).thenReturn(allEvents);
        assertEquals(allEvents, eventService.getAllEvents());
    }

    @Test
    void testGetEventsByDate() throws Exception {
        dayDTO.setEvents(allEvents);
        when(agendaEventRepository.findByDate(2017,1,13)).thenReturn(allEvents);
        assertEquals(dayDTO, eventService.getEventByDate(2017, 1, 13));
    }

    @Test
    void testGetEventsByDateWithLimit() throws Exception {
        dayDTO.setEvents(allEvents);
        when(agendaEventRepository.findByDate(2017,1,13, 2)).thenReturn(allEvents);
        assertEquals(dayDTO, eventService.getEventByDate(2017, 1, 13, 2));
    }

    @Test
    void testGetEventByDateAndGroupId() throws Exception {
        dayDTO.setEvents(group1Events);
        when(agendaEventRepository.findByDateAndGroupId(2017,1,13,1)).thenReturn(group1Events);
        assertEquals(dayDTO, eventService.getEventByDateAndGroupId(2017, 1, 13, 1));
    }

    @Test
    void testGetEventByDateAndGroupIdWithLimit() throws Exception {
        dayDTO.setEvents(group1Events);
        when(agendaEventRepository.findByDateAndGroupId(2017,1,13,1, 2)).thenReturn(group1Events);
        assertEquals(dayDTO, eventService.getEventByDateAndGroupId(2017, 1, 13, 1, 2));
    }

    @Test
    void testGetMonth() throws Exception {
        ArrayList<Integer> dayList = new ArrayList<>();
        ArrayList<DayDTO> expected = new ArrayList<>();
        dayList.add(0);
        expected.add(new DayDTO());
        dayList.add(0);
        expected.add(new DayDTO());
        for (int i = 1; i < 31; i++) {
            expected.add(new DayDTO(2021,6, i, new ArrayList<>()));
            dayList.add(i);
        }
        monthDTO.setDayList(expected);
        when(calendarService.getDaysinAMonth(2021,6)).thenReturn(dayList);
        lenient().when(agendaEventRepository.findByDate(
            eq(2021),eq(6), anyInt())).thenReturn(new ArrayList<>());
        assertEquals(monthDTO, eventService.getMonth(2021, 6));
    }

    @Test
    void testGetMonthByGroupId() throws Exception {
        ArrayList<Integer> dayList = new ArrayList<>();
        ArrayList<DayDTO> expected = new ArrayList<>();
        dayList.add(0);
        expected.add(new DayDTO());
        dayList.add(0);
        expected.add(new DayDTO());
        for (int i = 1; i < 31; i++) {
            expected.add(new DayDTO(2021,6, i, new ArrayList<>()));
            dayList.add(i);
        }
        monthDTO.setDayList(expected);
        when(calendarService.getDaysinAMonth(2021,6)).thenReturn(dayList);
        lenient().when(agendaEventRepository.findByDateAndGroupId(
            eq(2021),eq(6), anyInt(),eq(0))).thenReturn(new ArrayList<>());
        assertEquals(monthDTO, eventService.getMonthByGroupId(2021, 6,0));
    }

    @Test
    void testDateFormatter() throws Exception {
        assertEquals(
            LocalDate.parse("2021-05-07"), eventService.dateFormatter(2021,5,7));
    }

    @Test
    void testUpdateEvent() throws Exception {
        final String currentName = event.getName();
        event.setName("baru");
        when(eventService.updateEvent(event.getGroupId(), event)).thenReturn(event);
        Event result = eventService.updateEvent((long)1, event);
        assertEquals(event.getEventId(), result.getEventId());
        assertNotEquals(currentName, result.getName());
    }

    @Test
    void testDeleteEvent() {
        eventService.deleteEventByID(1L);
        eventService.getEventByEventId(event.getEventId());
        verify(agendaEventRepository, times(1)).delete(any());
    }

    @Test
    void testDeleteEventByGroupId() {
        when(agendaEventRepository.findByGroupIdOrderByDateAscTimeStartAsc(1))
            .thenReturn(group1Events);
        eventService.deleteEventByGroupId(1);
        verify(agendaEventRepository, times(group1Events.size())).delete(any());
    }

    @Test
    void testGetEventByGroupId() {
        when(agendaEventRepository.findByGroupIdOrderByDateAscTimeStartAsc(1))
            .thenReturn(group1Events);
        assertEquals(
            group1Events, eventService.getEventByGroupId(1));
        verify(agendaEventRepository, times(1))
            .findByGroupIdOrderByDateAscTimeStartAsc(1);
    }
}

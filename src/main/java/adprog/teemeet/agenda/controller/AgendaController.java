package adprog.teemeet.agenda.controller;

import adprog.teemeet.agenda.model.Event;
import adprog.teemeet.agenda.service.EventService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://localhost:8080", "https://teemeet.herokuapp.com"})
@RestController
@RequestMapping(path = "/events")
public class AgendaController {
    @Autowired
    private EventService eventService;

    @GetMapping("")
    @ResponseBody
    public ResponseEntity<List<Event>> getAllEvents() {
        return ResponseEntity.ok(eventService.getAllEvents());
    }

    /**
     * Handles GET request for search.
     * @return response entity month/day/event list
     */
    @GetMapping("/search")
    @ResponseBody
    public ResponseEntity getEventByDate(
        @RequestParam(value = "year", required = false) Integer year,
        @RequestParam(value = "month", required = false) Integer month,
        @RequestParam(value = "day", required = false) Integer day,
        @RequestParam(value = "groupId", required = false) Long groupId
    ) {
        if (day == null && groupId == null && month != null & year != null) {
            return ResponseEntity.ok(eventService.getMonth(year, month));
        } else if (day != null && groupId == null && month != null & year != null) {
            return ResponseEntity.ok(eventService.getEventByDate(year, month, day));
        } else if (day == null && groupId != null && month != null & year != null) {
            return ResponseEntity.ok(eventService.getMonthByGroupId(year, month, groupId));
        } else if (day == null && groupId != null && month == null & year == null) {
            return ResponseEntity.ok(eventService.getEventByGroupId(groupId));
        } else {
            return ResponseEntity.ok(
                    eventService.getEventByDateAndGroupId(year, month, day, groupId));
        }
    }

    @PostMapping("")
    @ResponseBody
    public ResponseEntity<Event> postEvent(@RequestBody Event event) {
        return ResponseEntity.ok(eventService.createEvent(event));
    }

    @GetMapping("/{eventId}")
    @ResponseBody
    public ResponseEntity<Event> getEventById(@PathVariable(value = "eventId") Long eventId) {
        return ResponseEntity.ok(eventService.getEventByEventId(eventId));
    }

    @PutMapping("/{eventId}")
    public ResponseEntity<Event> updateEvent(@PathVariable(value = "eventId") Long eventId,
                                             @RequestBody Event event) {
        return ResponseEntity.ok(eventService.updateEvent(eventId, event));
    }

    @DeleteMapping("/{eventId}")
    public void deleteEvent(@PathVariable(value = "eventId") Long eventId) {
        eventService.deleteEventByID(eventId);
    }

    @DeleteMapping("")
    public void deleteEventByGroupId(
        @RequestParam(value = "groupId") Long groupId) {
        eventService.deleteEventByGroupId(groupId);
    }
}
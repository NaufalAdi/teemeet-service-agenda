package adprog.teemeet.agenda.core;

import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MonthDTO {
    int year;
    int month;
    String monthName;
    List<DayDTO> dayList;

    /**
     * Constructor for MonthDTO.
     */
    public MonthDTO(int year, int month, String monthName, List<DayDTO> dayList) {
        this.year = year;
        this.month = month;
        this.monthName = monthName;
        this.dayList = dayList;
    }
}

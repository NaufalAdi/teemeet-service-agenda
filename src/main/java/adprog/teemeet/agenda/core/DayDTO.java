package adprog.teemeet.agenda.core;

import adprog.teemeet.agenda.model.Event;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class DayDTO {
    private int year;
    private int month;
    private int day;
    private List<Event> events;

    /**
     * Constructor for DayDTO.
     */
    public DayDTO(int year, int month, int day, List<Event> events) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.events = events;
    }

    public DayDTO() {
        this(0,0,0, new ArrayList<>());
    }
}

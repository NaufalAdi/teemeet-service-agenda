package adprog.teemeet.agenda.model;

import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "event")
@Data
@NoArgsConstructor
public class Event {
    @Id
    @Column(name = "event_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long eventId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "time_start")
    private LocalTime timeStart;

    @Column(name = "time_end")
    private LocalTime timeEnd;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "group_id")
    private long groupId;

    /**
     * Constructor for Event model.
     */
    public Event(String name, String desc, LocalDate date,
                 LocalTime timeStart, LocalTime timeEnd, long groupId) {
        this.name = name;
        this.description = desc;
        this.date = date;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.groupId = groupId;
    }
}

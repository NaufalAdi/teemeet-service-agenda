package adprog.teemeet.agenda.repository;

import adprog.teemeet.agenda.model.Event;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AgendaEventRepository extends JpaRepository<Event, Integer> {

    @Query(value = "SELECT * FROM Event e "
            + "WHERE EXTRACT(MONTH FROM date) = ?2 "
            + "and EXTRACT(YEAR FROM date) = ?1 "
            + "and EXTRACT(DAY FROM date) = ?3 "
            + "ORDER BY e.time_start",
            nativeQuery = true)
    List<Event> findByDate(int year, int month, int day);

    @Query(value = "SELECT * FROM Event e "
            + "where EXTRACT(MONTH FROM date) = ?2 "
            + "and EXTRACT(YEAR FROM date) = ?1 "
            + "and EXTRACT(DAY FROM date) = ?3 "
            + "ORDER BY e.event_id LIMIT ?4",
            nativeQuery = true)
    List<Event> findByDate(int year, int month, int day, int limit);

    @Query(value = "SELECT * FROM Event e "
            + "where EXTRACT(MONTH FROM date) = ?2 "
            + "and EXTRACT(YEAR FROM date) = ?1 "
            + "and EXTRACT(DAY FROM date) = ?3 "
            + "and e.group_id = ?4 "
            + "ORDER BY e.time_start ",
            nativeQuery = true)
    List<Event> findByDateAndGroupId(int year, int month, int day, long groupId);

    @Query(value = "SELECT * FROM Event e "
            + "where EXTRACT(MONTH FROM date) = ?2 "
            + "and EXTRACT(YEAR FROM date) = ?1 "
            + "and EXTRACT(DAY FROM date) = ?3 "
            + "and e.group_id = ?4 "
            + "ORDER BY e.event_id LIMIT ?5",
            nativeQuery = true)
    List<Event> findByDateAndGroupId(int year, int month, int day, long groupId, int limit);

    List<Event> findByGroupIdOrderByDateAscTimeStartAsc(long groupId);

    Event getEventByEventId(long id);

}

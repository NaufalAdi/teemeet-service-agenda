package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.core.DayDTO;
import adprog.teemeet.agenda.core.MonthDTO;
import adprog.teemeet.agenda.model.Event;
import adprog.teemeet.agenda.repository.AgendaEventRepository;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl implements EventService {
    @Autowired
    AgendaEventRepository agendaEventRepository;

    @Autowired
    CalendarService calendarService;

    @Override
    public Event createEvent(Event event) {
        Event newEvent = new Event(event.getName(), event.getDescription(),
                event.getDate(), event.getTimeStart(), event.getTimeEnd(), event.getGroupId());
        agendaEventRepository.save(newEvent);
        return newEvent;
    }

    @Override
    public Event getEventByEventId(Long eventId) {
        return agendaEventRepository.getEventByEventId(eventId);
    }

    @Override
    public List<Event> getAllEvents() {
        return agendaEventRepository.findAll();
    }

    @Override
    public DayDTO getEventByDate(int year, int month, int day) {
        return new DayDTO(year, month, day,
                agendaEventRepository.findByDate(year, month, day));
    }

    @Override
    public DayDTO getEventByDate(int year, int month, int day, int limit) {
        return new DayDTO(year, month, day,
                agendaEventRepository.findByDate(year, month, day, limit));
    }

    @Override
    public DayDTO getEventByDateAndGroupId(int year, int month, int day, long groupId) {
        return new DayDTO(year, month, day,
                agendaEventRepository.findByDateAndGroupId(year, month, day, groupId));
    }

    @Override
    public DayDTO getEventByDateAndGroupId(int year, int month, int day, long groupId, int limit) {
        return new DayDTO(year, month, day,
                agendaEventRepository.findByDateAndGroupId(year, month, day, groupId, limit));
    }

    @Override
    public MonthDTO getMonthByGroupId(int year, int month, long groupId) {
        List<DayDTO> res = new ArrayList<>();
        for (int day :calendarService.getDaysinAMonth(year,month)) {
            if (day != 0) {
                res.add(getEventByDateAndGroupId(year, month, day, groupId));
            } else {
                res.add(new DayDTO());
            }
        }
        var monthName = Month.of(month).toString();
        return new MonthDTO(year, month, monthName, res);
    }

    @Override
    public MonthDTO getMonth(int year, int month) {
        List<DayDTO> res = new ArrayList<>();
        for (int day :calendarService.getDaysinAMonth(year,month)) {
            if (day != 0) {
                res.add(getEventByDate(year, month, day));
            } else {
                res.add(new DayDTO());
            }
        }
        var monthName = Month.of(month).toString();
        return new MonthDTO(year, month, monthName, res);
    }

    @Override
    public LocalDate dateFormatter(int year, int month, int day) {
        DecimalFormat formatter = new DecimalFormat("00");
        String dayFormatted = formatter.format(day);
        String monthFormatted = formatter.format(month);
        String dateString = year + "-" + monthFormatted + "-" + dayFormatted;
        return LocalDate.parse(dateString);
    }

    @Override
    public Event updateEvent(Long eventId, Event event) {
        event.setEventId(eventId);
        agendaEventRepository.save(event);
        return event;
    }

    @Override
    public List<Event> getEventByGroupId(long groupId) {
        return agendaEventRepository.findByGroupIdOrderByDateAscTimeStartAsc(groupId);
    }

    @Override
    public void deleteEventByID(long id) {
        Event event = getEventByEventId(id);
        agendaEventRepository.delete(event);
    }

    @Override
    public void deleteEventByGroupId(long groupId) {
        List<Event> events =
            agendaEventRepository.findByGroupIdOrderByDateAscTimeStartAsc(groupId);
        for (Event event:events) {
            agendaEventRepository.delete(event);
        }
    }
}



package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.core.DayDTO;
import adprog.teemeet.agenda.core.MonthDTO;
import adprog.teemeet.agenda.model.Event;
import java.time.LocalDate;
import java.util.List;

public interface EventService {
    Event createEvent(Event event);

    Event getEventByEventId(Long eventId);

    List<Event> getAllEvents();

    DayDTO getEventByDate(int year, int month, int day);

    DayDTO getEventByDate(int year, int month, int day, int limit);

    DayDTO getEventByDateAndGroupId(int year, int month, int day, long groupId);

    DayDTO getEventByDateAndGroupId(int year, int month, int day, long groupId, int limit);

    MonthDTO getMonthByGroupId(int year, int month, long groupId);

    MonthDTO getMonth(int year, int month);

    LocalDate dateFormatter(int year, int month, int day);

    Event updateEvent(Long eventId,Event event);

    List<Event> getEventByGroupId(long groupId);

    void deleteEventByID(long id);

    void deleteEventByGroupId(long groupId);

}
